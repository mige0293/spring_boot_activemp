package com.example.spring_boot_activemp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootActivempApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootActivempApplication.class, args);
    }

}
