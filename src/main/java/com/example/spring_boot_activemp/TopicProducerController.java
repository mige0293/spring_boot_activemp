package com.example.spring_boot_activemp;


import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Destination;
import javax.jms.Topic;
import java.util.ArrayList;

/*
 * topic訊息生產者
 */
@RestController
public class TopicProducerController {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private Topic topic;

    private ArrayList<String> topicArr;

    /*
     * 訊息生產者
     */
    @RequestMapping("/topicsendmsg")
    public void sendmsg(String topic , String msg) {

        //https://www.796t.com/article.php?id=18089
        //詳解Springboot整合ActiveMQ（Queue和Topic兩種模式）

        //https://blog.csdn.net/weixin_41549393/article/details/116273105
        //SpringBoot集成ActiveMQ-＞MQTT


        Destination destination = new ActiveMQTopic(topic);
        JmsTemplate jmsTemplate = jmsMessagingTemplate.getJmsTemplate();
        jmsTemplate.setPubSubDomain(true); //使否往下傳送
        try {
            jmsMessagingTemplate.convertAndSend(destination, msg);
        } catch (MessagingException e) {
            System.out.println(e.getMessage());
        }

    }
}
